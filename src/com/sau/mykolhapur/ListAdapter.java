package com.sau.mykolhapur;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListAdapter extends BaseAdapter {
	
	Context context;
	private static LayoutInflater inflater=null;
	private ArrayList<ObjItem> objItems;
	
	
	public ListAdapter(Context context , ArrayList<ObjItem> objItem) {
		this.context = context;
		this.objItems = objItem;
	}
	
	@Override
	public int getCount() {
		return objItems.size();
	}

	@Override
	public Object getItem(int position) {
		return objItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return (objItems.get(position).index);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi=convertView;
		
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(convertView==null)
			vi = inflater.inflate(R.layout.home_list_item, null);
		
		ImageView img = (ImageView) vi.findViewById(R.id.img_news);
		TextView txt = (TextView) vi.findViewById(R.id.lbl_news);
		
		if(objItems.get(position).type == 1)
			img.setImageResource(R.drawable.event_flag);
		else
			img.setImageResource(R.drawable.news_icon);
		
		txt.setText(objItems.get(position).headline);
		
		return vi;
	}
	
}
