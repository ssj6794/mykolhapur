package com.sau.mykolhapur;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.gson.Gson;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Home extends Activity implements OnRefreshListener {
	
	private PullToRefreshLayout mPullToRefreshLayout;
	private getJSON worker = new getJSON();
	SharedPreferences prefs;
	ListView list;
	ListAdapter adapter;
	Context context;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        setContentView(R.layout.activity_home);
        context = this;
        
        Bundle extras = getIntent().getExtras();
        
        setTitle(R.string.app_name);
        
        mPullToRefreshLayout = (PullToRefreshLayout) findViewById(R.id.ptr_layout);
        
        ActionBarPullToRefresh.from(this)
        // Mark All Children as pullable
        .allChildrenArePullable()
        // Set the OnRefreshListener
        .listener(this)
        // Finally commit the setup to our PullToRefreshLayout
        .setup(mPullToRefreshLayout);
        
        
        list = (ListView) findViewById(R.id.list);
        
        if(extras != null || prefs.getInt("ref_req", 0) == 1){
        	Editor edit = prefs.edit();
        	edit.putInt("ref_req", 0);
        	edit.commit();
        	worker = new getJSON();
            worker.execute();
        }
        else
        	loadCache();
         
        
        final Intent i = new Intent(this,Article.class);
        
        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            	
            	if(arg3 != -1)
            	i.putExtra("Index",prefs.getInt("ITEMCOUNT", 0) - arg2);
            	else
            		i.putExtra("Index",prefs.getInt("ITEMCOUNT", 0));
            	
            	startActivity(i);
            	overridePendingTransition(R.animator.push_down, R.animator.slide_up);
            }});
    }
    
    @Override
    public void onStart() {
      super.onStart();
      EasyTracker.getInstance(this).activityStart(this);
    }
    
    @Override
    public void onStop() {
      super.onStop();
      EasyTracker.getInstance(this).activityStop(this);
    }
    
    private Bitmap loadImage(){
    	String filename = Environment.getExternalStorageDirectory().toString() + "/my_kolhapur/latest.jpg";
    	try{
    		Bitmap img = BitmapFactory.decodeFile(filename);
    		return img;
    	}
    	catch(Exception e){return null;}
    	
    }
    
    private void loadCache(){
    	int count = prefs.getInt("ITEMCOUNT", 0);
    	ArrayList<ObjItem> objlist = new ArrayList<ObjItem>();
    	
    	if(count!=0)
    	{
    		for(int i = 0 ;  i < count ; i++)
    			objlist.add( new Gson().fromJson(prefs.getString("ITEM"+i, ""), ObjItem.class));
    		
    		Collections.sort(objlist, new Comparator<ObjItem>() {
		        @Override
		        public int compare(ObjItem ite1, ObjItem ite2)
		        {
		            return Integer.valueOf(ite1.index).compareTo(Integer.valueOf(ite2.index));
		        }
		    });
    		//Reverse list to get latest on top
    		Collections.reverse(objlist);
    		
    		View header = (View)getLayoutInflater().inflate(R.layout.list_top_item,null);
			 
			 list.setAdapter(null);
			 
			 //No articles
			 if(objlist.size() == 0){
				 mPullToRefreshLayout.setRefreshComplete();
				 return;	 
			 }
			 
			 ImageView img = (ImageView) header.findViewById(R.id.ImageView);
			 Bitmap b = loadImage();
			 
			 if(b != null)
				 img.setImageBitmap(b);
			 
			 TextView txt = (TextView) header.findViewById(R.id.ImageViewText);
			 txt.setText(objlist.get(0).headline);
			 objlist.remove(0);
			
			 if(list.getHeaderViewsCount() < 1)
				 list.addHeaderView(header);
			 
			 if(objlist.size() >= 1){
				 adapter = new ListAdapter(context,objlist);
				 list.setAdapter(adapter);
				 System.out.println("Settings adapter");
			 }
			 else
				 list.setAdapter(null);
    	}
    	
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.action_settings:
        	
        	Editor editor = prefs.edit();
        	
        	if(prefs.getString("Locale", "en").equals("mr"))
        		editor.putString("Locale","en");
        	else
        		editor.putString("Locale","mr");
        	
        	editor.commit();
        	startActivity(new Intent(this,Splash.class));
        	return true;
        	
        	
        case R.id.action_register:
        	Editor edit = prefs.edit();
        	edit.putInt("FirstRun", 0);
        	edit.commit();
        	startActivity(new Intent(this,Splash.class));
        	return true;
        	
            
        default:
                return super.onOptionsItemSelected(item);
        }
    }

	@Override
	public void onRefreshStarted(View view) {
		if(!(worker.getStatus() == AsyncTask.Status.RUNNING))
		{
			worker = new getJSON();
			worker.execute();
		}
		
	}
	
	 public class getJSON extends AsyncTask<Void,Void,Void>{
		 ArrayList<ObjItem> objlist;
		 Bitmap bitmap;
		 
		 private void SaveImage(Bitmap finalBitmap) {

			    String root = Environment.getExternalStorageDirectory().toString();
			    File myDir = new File(root + "/my_kolhapur");    
			    myDir.mkdirs();
			    String fname = "latest.jpg";
			    File file = new File (myDir, fname);
			    if (file.exists ()) file.delete (); 
			    try {
			           FileOutputStream out = new FileOutputStream(file);
			           finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
			           out.flush();
			           out.close();

			    } catch (Exception e) {
			           e.printStackTrace();
			    }
			}
		 
		 @Override
		 protected void onPreExecute(){
			 //list.removeAllViews();
			 objlist = new ArrayList<ObjItem>();
			 objlist.clear();
			 mPullToRefreshLayout.setRefreshing(true);
		 }
			
		 @Override
		 protected Void doInBackground(Void... params) {
			 try {
					URL url = new URL("http://kolhapur-now.appspot.com/getallitems");
					HttpURLConnection con = (HttpURLConnection) url.openConnection();
					InputStream stream = con.getInputStream();
					StringBuilder sb = new StringBuilder();
					String line;
					BufferedReader br = new BufferedReader(new InputStreamReader(stream));
					
					while ((line = br.readLine()) != null) {
						sb.append(line);
					}
					br.close();
					
					String json = sb.toString();
					
					JSONObject root = new JSONObject(json);
					
					if(root.has("Data"))
					{
						JSONArray data = root.getJSONArray("Data");
						Editor editor = prefs.edit();
						
						for(int i = 0 ; i < data.length() ; i++){
							JSONObject temp = data.getJSONObject(i);
							ObjItem obj = new ObjItem();
							obj.putVals(temp);
							objlist.add(obj);
							
							Gson gson = new Gson();
							System.out.println("Now Puttin ITEM"+i);
							editor.putString("ITEM"+i, gson.toJson(obj));
						}
						editor.putInt("ITEMCOUNT", data.length());
						editor.commit();
					}
					
					Collections.sort(objlist, new Comparator<ObjItem>() {
				        @Override
				        public int compare(ObjItem ite1, ObjItem ite2)
				        {
				            return Integer.valueOf(ite1.index).compareTo(Integer.valueOf(ite2.index));
				        }
				    });
					
					if(objlist.size() > 0){
						bitmap = BitmapFactory.decodeStream((InputStream)new URL(objlist.get(objlist.size() - 1).img_url).getContent());
						SaveImage(bitmap);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			 return null;
			
		 }
		 
		 @Override
		 protected void onPostExecute(Void params){
			 Toast.makeText(context, "Refreshed", Toast.LENGTH_SHORT).show();
			 loadCache();
			 mPullToRefreshLayout.setRefreshComplete();
		 }
			
			
		}
}
