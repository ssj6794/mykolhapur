package com.sau.mykolhapur;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.util.Linkify;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Article extends Activity {
	TextView txt_loading;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_article);
		ActionBar actionBar = getActionBar();
	    actionBar.setDisplayHomeAsUpEnabled(true);
	    
		setTitle(R.string.app_name);
		TextView txt_main = (TextView) findViewById (R.id.txt_main);
		TextView txt_headline = (TextView) findViewById (R.id.txt_headline);
		ImageView img_article = (ImageView) findViewById(R.id.img_article);
		txt_loading = (TextView) findViewById(R.id.lbl_loading);
		Bundle extras = getIntent().getExtras();
		
		SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(this);
		Gson gson = new Gson();
		int index = extras.getInt("Index")-1;
		
			
			
		String json = prefs.getString("ITEM"+ index, "");
		ObjItem obj = gson.fromJson(json, ObjItem.class);
		
		
		if(prefs.getInt("ITEMCOUNT", 0) == index + 1){
			Bitmap b = loadImage("latest.jpg");
			 if(b != null)
				 img_article.setImageBitmap(b);
		}
		else{
			Ion.with(this)
	        .load(obj.img_url)
	        .intoImageView(img_article)
	        .setCallback(new FutureCallback<ImageView>() {
				@Override
				public void onCompleted(Exception arg0, ImageView arg1) {
					if(arg0 != null)
						txt_loading.setText("Error loading image");
					else
						txt_loading.setVisibility(View.GONE);
				}
			});
		}
			
		txt_headline.setText(obj.headline);
		txt_main.setText(obj.body);
		Linkify.addLinks(txt_main, Linkify.ALL);
	
		}
	
	 @Override
	    public void onStart() {
	      super.onStart();
	      EasyTracker.getInstance(this).activityStart(this);
	    }
	    
	    @Override
	    public void onStop() {
	      super.onStop();
	      EasyTracker.getInstance(this).activityStop(this);
	    }
	    
	 private Bitmap loadImage(String img_name){
	    	String filename = Environment.getExternalStorageDirectory().toString() + "/my_kolhapur/" + img_name;
	    	try{
	    		Bitmap img = BitmapFactory.decodeFile(filename);
	    		return img;
	    	}
	    	catch(Exception e){return null;}
	    	
	    }
	
}
