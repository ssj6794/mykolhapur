package com.sau.mykolhapur;

import java.util.Locale;

import com.collegecode.gcmapi.PlayServicesHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

public class Splash extends Activity {
	SharedPreferences prefs;
	Context context;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        context = this;
        
        if(prefs.getInt("FirstRun", 0) == 0)
        	new registerDevice().execute();
        
        else{
        	if (prefs.getString("Locale", "en").equals("en"))
        	{
        		System.out.println("Setting to default");
				setLocale(Locale.getDefault().toString());}
			else
				setLocale("mr");
        }
	}
	
	public void setLocale(String lang) {
		System.out.println("Set locale of app to " + lang);
    	Locale myLocale = new Locale(lang);
    	//Locale current = getResources().getConfiguration().locale;
    	Resources res = getResources(); 
		DisplayMetrics dm = res.getDisplayMetrics(); 
		Configuration conf = res.getConfiguration();
    	conf.locale = myLocale; 
    	res.updateConfiguration(conf, dm);
    	Intent refresh = new Intent(context, Home.class); 
    	refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    	startActivity(refresh);
    	} 
	
	
	
	public class registerDevice extends AsyncTask<Void,Void,Void>{
		ProgressDialog pd;
		
		protected void onPreExecute(){
			pd = new ProgressDialog(context);
			pd.setCancelable(false);
			pd.setMessage("Loading");
			pd.setIndeterminate(true);
			pd.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			PlayServicesHandler ps = new PlayServicesHandler(getApplicationContext());
			try {
				ps.RegisterDevice();
				System.out.println("Done");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void params){
			if(pd.isShowing())
				pd.dismiss();
			Editor editor = prefs.edit();
			editor.putInt("FirstRun", 1);
			editor.commit();
			
			if (prefs.getString("Locale", "en") == "en")
				setLocale(Locale.getDefault().toString()); 
			else
				setLocale("mr");   
		}
		
		public void setLocale(String lang) { 
			System.out.println("Set locale of app to " + lang);
	    	Locale myLocale = new Locale(lang);
	    	Locale current = getResources().getConfiguration().locale;
	    	if(current != Locale.getDefault())
	    		myLocale = Locale.getDefault();	
	    	Resources res = getResources(); 
			DisplayMetrics dm = res.getDisplayMetrics(); 
			Configuration conf = res.getConfiguration();
	    	conf.locale = myLocale; 
	    	res.updateConfiguration(conf, dm);
	    	Intent refresh = new Intent(context, Home.class); 
	    	refresh.putExtra("OHHH", "KDUDE");
	    	refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    	startActivity(refresh);
	    	} 
		}
	
	
	
	
}



