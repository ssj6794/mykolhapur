package com.sau.mykolhapur;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

public class ObjItem {
	public int index;
	public int type;
	public String headline;
	public String body;
	public String img_url;
	
	
	public void putVals(JSONObject j){
		try {
			index = j.getInt("index");
			headline = (String) j.get("headline");
			type = j.getInt("type");
			body = getBody((String) j.get("body"));
			System.out.println("GOT BODY: " + body);
			img_url = (String) j.get("img");
		
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private String getBody(String url1){
		StringBuilder sb = new StringBuilder();
		URL url;
		try {
			url = new URL(url1);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			
			InputStream stream = con.getInputStream();
			String line;
			BufferedReader br = new BufferedReader(new InputStreamReader(stream));
			while ((line = br.readLine()) != null) {
				sb.append(System.getProperty("line.separator"));
				sb.append(line);
			}
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return sb.toString();
	}
}
