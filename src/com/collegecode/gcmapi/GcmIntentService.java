package com.collegecode.gcmapi;

import java.util.Calendar;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.sau.mykolhapur.Home;
import com.sau.mykolhapur.R;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = (int) Calendar.getInstance().getTimeInMillis();
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    String TAG = "com.collegecode.GcmIntentService";
    
    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
               // sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
              //  sendNotification("Deleted messages on server: " +
                       // extras.toString());
            // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
            	if(extras.containsKey("refresh_push")){
            		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            		Editor edit = prefs.edit();
            		edit.putInt("ref_req", 1);
            		edit.commit();
            		GcmBroadcastReceiver.completeWakefulIntent(intent);
            		return;
            		
            	}
            	
                if(Integer.parseInt(extras.getString("type")) == 1)
                	sendNotification(extras.getString("headline"), "New Event!", R.drawable.flag_notification);
                else
                	sendNotification(extras.getString("headline"), "New News!", R.drawable.news_notification);
                // Post notification of received message.
                
                Log.i(TAG, "Received: " + extras.toString());
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    
    private void sendNotification(String title , String msg, int pic) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent i = new Intent(this,Home.class);
        i.putExtra("OHHH", "KDUDE");
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,i, Intent.FLAG_ACTIVITY_NEW_TASK);
        NotificationCompat.Builder mBuilder =
        		new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.ic_launcher)
        .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),pic))
        .setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_VIBRATE| Notification.DEFAULT_SOUND)
        .setContentTitle(title)
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(msg))
        .setAutoCancel(true)
        .setContentText(msg);
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
