package com.collegecode.gcmapi;

import java.net.HttpURLConnection;
import java.net.URL;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class PlayServicesHandler {
	Context context;
	
	GoogleCloudMessaging gcm;
	
	static final String TAG = "com.collegecode.gcmapi";
	
	String SENDER_ID = "147379121243";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	
	public PlayServicesHandler(Context context){
		this.context = context;
	}
	
	/**
	 * Check the device to make sure it has the Google Play Services APK. If
	 * it doesn't, display a dialog that allows users to download the APK from
	 * the Google Play Store or enable it in the device's system settings.
	 */
	public boolean checkPlayServices() {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
	    if (resultCode != ConnectionResult.SUCCESS) {
	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
	            GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity)context,
	                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
	        } else {
	            Log.i(TAG, "This device is not supported.");
	        }
	        return false;
	    }
	    return true;
	}
	
	public void RegisterDevice() throws Exception{
		try{
			gcm = GoogleCloudMessaging.getInstance(context);
			String regid = gcm.register(SENDER_ID);
			RegisterWithServer(regid);
			storeRegistrationId(regid);
			
			}
		catch(Exception e){throw new Exception("Error!");}
	}
	
	private void RegisterWithServer(String regid){
		try {
			URL url = new URL("http://kolhapur-now.appspot.com/kolhapurnow?register=1&devId=" + regid);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			System.out.println("Response Code : " + con.getResponseCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void storeRegistrationId(String regId) {
	    final SharedPreferences prefs = getGCMPreferences();
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString("PROPERTY_REG_ID", regId);
	    editor.commit();
	}
	
	private SharedPreferences getGCMPreferences() {
	    
	    return PreferenceManager.getDefaultSharedPreferences(context);
	}

}
